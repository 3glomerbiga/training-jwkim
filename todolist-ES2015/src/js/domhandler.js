
import CONST from './constant.js';
import util from './util.js';

export default class DomHandler {
    /**
     * initialize
     * @param {Object} domElements - event target elements
     */
    constructor(domElements) {
        let chkDomElement = [];
        Object.keys(domElements).forEach(function(nodeName) {
            if (CONST.DOM_ELEMENTS.indexOf(nodeName) > -1) {
                chkDomElement.push(nodeName);
            }
        });
        if (chkDomElement.length < CONST.DOM_ELEMENTS.length) {
            throw new Error('needDomElement');
        }
        Object.assign(this, domElements);
    }
    /**
     * print count info
     * @param {number} uncompleteCount - left tode count
     * @param {number} completeCount - completed count
     */
    printInfoCountElement(uncompleteCount, completeCount) {
        this.uncompleteCount.innerHTML = uncompleteCount;
        this.completeCount.innerHTML = completeCount;
    }
    /**
     * count print
     * @param {Array.<TodoItem>} todoList - todolist Array
     */
    printTodoListElement(todoList) {
        let listArrayLength = todoList.length;
        let htmlStringBuffer = [];
        let todo = null;
        let i = 0;
        let checked = '';
        let liHtml = [
            '<li class="{$checked}">',
            '<label>',
            '<input type="checkbox" name="seq" value="{$seq}" {$checked} />',
            '</label>',
            '<span>{$todoname}</span>',
            '</li>'
        ].join('');

        for (; i < listArrayLength; i += 1) {
            todo = todoList[i];
            checked = (todo.complete) ? 'checked' : '';

            htmlStringBuffer.push(util.template(liHtml, {
                'seq': todo.seq,
                'checked': checked,
                'todoname': todo.todoname
            }));
        }
        this.list.innerHTML = htmlStringBuffer.join('\n');
    }
    /**
     * filter type display (all, completed, uncompleted)
     * @param {number} filterType - filterType (0: ALL, 1: COMPLETE, 2: UNCOMPLETE )
     */
    printFilterElement(filterType) {
        let spanNode = this.filterButton.querySelectorAll('span');
        let length = spanNode.length;
        let i = 0;
        for (; i < length; i += 1) {
            util.removeClass(spanNode[i], 'active');
            if (Number(spanNode[i].getAttribute('data-filtertype')) === filterType) {
                util.addClass(spanNode[i], 'active');
            }
        }
    }
    /**
     * input area enter key event handler
     * @param {Function} callback - Callback after event handling.
     */
    onInputKeyDownHandler(callback) {
        util.addEventHandler(this.input, 'keydown', function(event) {
            let eventTarget = util.findEventTarget(event);
            event = event || window.event;
            if (event.keyCode === CONST.ENTER_KEYCODE) {
                if (callback) {
                    callback(eventTarget.value);
                }
                eventTarget.value = '';
            }
        });
    }
    /**
     * todolist checkbox event
     * @param {Function} callback - Callback after event handling.
     */
    onTodoCheckBoxClickHandler(callback) {
        util.addEventHandler(this.list, 'click', function(event) {
            let eventTarget = util.findEventTarget(event);
            if (eventTarget.tagName === 'INPUT') {
                if (callback) {
                    callback(Number(eventTarget.value));
                }
            }
        });
    }
    /**
     * todo remove button event handler
     * @param {Function} callback - Callback after event handling.
     */
    onRemoveButtonClickHandler(callback) {
        util.addEventHandler(this.removeTodoButton, 'click', function() {
            if (callback) {
                callback();
            }
        });
    }
    /**
     * filter state change event handler
     * @param {Function} callback - Callback after event handling.
     */
    onFilterChangeButtonClickHandler(callback) {
        let template = [
            '<span data-filtertype="0" class="active">all</span>',
            '<span data-filtertype="2">active</span>',
            '<span data-filtertype="1">completed</span>'
        ].join('');
        this.filterButton.innerHTML = template;

        util.addEventHandler(this.filterButton, 'click', (event) => {
            let eventTarget = util.findEventTarget(event);
            let newFilterType = Number(eventTarget.getAttribute('data-filtertype'));
            if (callback) {
                callback(newFilterType);
            }
        });
    }
}

